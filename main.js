let button = [...document.querySelectorAll(".btn")];

function setListenerButton() {
  document.querySelector("body").addEventListener("keydown", (event) => {
    for (elem of button) {
      elem.style.backgroundColor = "#000000";
    }
    let [symbol] = button.filter((but) =>
        `Key${but.innerText}` == event.code ||`Numpad${but.innerText}` == event.code || but.innerText == event.code);

    if (typeof symbol !== "undefined") symbol.style.backgroundColor = "#413eed";
  });
}

setListenerButton();
